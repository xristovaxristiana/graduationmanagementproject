<?php 
class StudentModel extends BaseModel {
    private $table_import;
    private $table_attended;
    private $table_confirmation;
    private $table_caps;
    private $table_gowns;

    public $id;
    public $name;
    public $surname;
    public $fn;
    public $specialty;
    public $degree;
    public $grade;
	public $username;

    public function __construct(){
      parent::__construct();
      $this->table_import = "students_import";
      $this->table_attended = "attended";
      $this->table_confirmation = "confirmation";
      $this->table_caps = "caps";
      $this->table_gowns = "gowns";
    }
	
	public function importStudents() {
      $sql = "INSERT INTO $this->table_import (name, surname, fn, specialty, degree, grade, username) VALUES (:name, :surname, :fn, :specialty, :degree, :grade, :username);";

      $query = $this->connection->prepare($sql);
	  
      $query->bindParam(":name", $this->name);
      $query->bindParam(":surname", $this->surname);
      $query->bindParam(":fn", $this->fn);
      $query->bindParam(":specialty", $this->specialty);
      $query->bindParam(":degree", $this->degree);
      $query->bindParam(":grade", $this->grade);
      $query->bindParam(":username", $this->username);

      return $query->execute();
    }
	
	public function importDefaultAttended() {
      $sql = "INSERT INTO $this->table_attended (attended, username) VALUES (0, :username);";
      $query = $this->connection->prepare($sql);
      $query->bindParam(":username", $this->username);
      return $query->execute();
    }
	
	public function importDefaultConfirmation() {
      $sql = "INSERT INTO $this->table_confirmation (confirmed, username) VALUES (0, :username);";
      $query = $this->connection->prepare($sql);
      $query->bindParam(":username", $this->username);
      return $query->execute();
    }
	
	public function importDefaultCaps() {
      $sql = "INSERT INTO $this->table_caps (recieved, fn) VALUES (0, :fn);";
      $query = $this->connection->prepare($sql);
      $query->bindParam(":fn", $this->fn);
      return $query->execute();
    }
	
	public function importDefaultGowns() {
      $sql = "INSERT INTO $this->table_gowns (recieved, fn) VALUES (0, :fn);";
      $query = $this->connection->prepare($sql);
      $query->bindParam(":fn", $this->fn);
      return $query->execute();
    }
	
	public function getFnFromUsername() { 
	 $sql = "SELECT fn FROM $this->table_import WHERE username LIKE :username;";
	 $query = $this->connection->prepare($sql);
	 $query->bindParam(":username", $this->username);
	 $query->execute();
     $userData = $query->fetch(PDO::FETCH_ASSOC)['fn'];
     return $userData;
	}
}
?>