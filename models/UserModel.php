<?php 
class UserModel extends BaseModel {
    private $table;

    public $id;
    public $username;
    public $password;
	public $role;

    public function __construct(){
      parent::__construct();
      $this->table = "users";
    }
	
	public function addUser() {
      $sql = "INSERT INTO $this->table (username, password, role) VALUES (:username, :password, :role);";

      $query = $this->connection->prepare($sql);

      $pass = password_hash($this->password, PASSWORD_DEFAULT);

      $query->bindParam(":username", $this->username);
      $query->bindParam(":password", $pass);
      $query->bindParam(":role", $this->role);

      $query->execute();
    }
	
	public function getUser() {
      $sql = "SELECT id, username, password, role FROM $this->table WHERE username LIKE :username;";
      $query = $this->connection->prepare($sql);
      $query->bindParam(":username", $this->username);

      $query->execute();
      $userData = $query->fetch(PDO::FETCH_ASSOC);
      return $userData;
    }
	
	public function getAllUsers($role) {
	  $sql = "";
	  
	  if($role === 'all'){
		$sql = "SELECT id, username, password, role FROM $this->table;";  
	  } else {
		 $sql = "SELECT id, username, password, role FROM $this->table WHERE role LIKE :role;";
	  }
	  
      $query = $this->connection->prepare($sql);
      $query->bindParam(":role", $role);

      $query->execute();
      $usersData = $query->fetchAll(PDO::FETCH_ASSOC);
      return $usersData;
    }
	
	public function grantUserAdministrativeAccess() {
	  $sql = "UPDATE $this->table SET role = 'administrator' WHERE username LIKE :username;";
      $query = $this->connection->prepare($sql);
      $query->bindParam(":username", $this->username);

      $query->execute();
    }
	
	public function deleteAccount() {
	  $sql = "DELETE FROM $this->table WHERE username LIKE :username;";
      $query = $this->connection->prepare($sql);
      $query->bindParam(":username", $this->username);

      $query->execute();
    }
	
	public function resetAccountPassword() {
	  $sql = "UPDATE $this->table SET password = :password WHERE username LIKE :username;";
      $query = $this->connection->prepare($sql);
	  
	  $pass = password_hash($this->password, PASSWORD_DEFAULT);
	  
      $query->bindParam(":username", $this->username);
      $query->bindParam(":password", $pass);

      $query->execute();
    }
    
}

?>