<?php 
class DocumentsModel extends BaseModel {
	private $gownsTable;
	private $capsTable;
	private $graduatesTable;

    public function __construct(){
      parent::__construct();
      $this->gownsTable = "gowns";
	  $this->capsTable = "caps";
	  $this->graduatesTable = "students_import";
    }

	public function getGownsData() {		
		$sql = "SELECT id,recieved,fn FROM $this->gownsTable";
		$query = $this->connection->prepare($sql);
		$query->execute();
		$gownsData = $query->fetchAll(PDO::FETCH_ASSOC);
		return $gownsData;
	}
	
	public function getCapsData() {		
		$sql = "SELECT id,recieved,fn FROM $this->capsTable";
		$query = $this->connection->prepare($sql);
		$query->execute();
		$capsData = $query->fetchAll(PDO::FETCH_ASSOC);
		return $capsData;
	}
	
	public function getGraduatesData($column) {		
		$sql = "";
		if($column=="grade"){
			$sql = "SELECT name,surname,fn,specialty,degree,grade FROM $this->graduatesTable ORDER BY grade DESC";
		}elseif($column=="specialty"){
			$sql = "SELECT name,surname,fn,specialty,degree,grade FROM $this->graduatesTable ORDER BY specialty ASC";
		}else{
			$sql = "SELECT name,surname,fn,specialty,degree,grade FROM $this->graduatesTable ORDER BY degree ASC";
		}
		$query = $this->connection->prepare($sql);
		$query->execute();
		$graduatesData = $query->fetchAll(PDO::FETCH_ASSOC);
		return $graduatesData;
	}	
}
?>