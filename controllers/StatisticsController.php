<?php

class StatisticsController extends BaseController {
	
	public function loadStatistics(){
		if(UserController::isLogged()) {
			$success = array();
		
			if (UserController::isLogged() && UserController::isAdministrator()) {
				$statistics = new StatisticsModel();				
				array_push($success, $statistics->getCountOfConfirmed()['COUNT']);
				array_push($success, $statistics->getCountOfAttended()['COUNT']);
				array_push($success, $statistics->getCountOfReceivedGowns()['COUNT']);
				array_push($success, $statistics->getCountOfReceivedCaps()['COUNT']);
				array_push($success, $statistics->getCountOfAwarded()['COUNT']);
				BaseController::load('/statisticsAdministrator.php', array(), $success);
				
			} else if (UserController::isLogged() && !UserController::isAdmin() && !UserController::isAdministrator()){
				$statistics = new StatisticsModel();
				array_push($success, $statistics->getUserConfirmation());
				array_push($success, $statistics->getUserAttendance());
				array_push($success, $statistics->getUserReceivedGowns());
				array_push($success, $statistics->getUserReceivedCaps());
				array_push($success, $statistics->getUserAwarded());
				BaseController::load('/statisticsStudent.php', array(), $success);
			}
			
        } else {
            BaseController::load('/login.php');
        }  
	}
	
}
?>