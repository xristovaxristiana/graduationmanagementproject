<!DOCTYPE html>
<html lang="bg">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Graduation Portal - Login</title>

    <link rel="stylesheet" href=<?php echo ROOT."views/css/style.css"?>>
</head>

<body class="background-auth">
    <div class="auth">
        <form class="auth-form" method="POST" action="<?php echo LOCATION.'login'?>" autocomplete="on"> 
            <h1 class="page-title">Портал за дипломирани студенти</h1> 
			<h2 class="page-subtitle">Вход</h2>
					
			<?php include_once VIEWS_DIR.'/errors.php'; ?>
                    
            <input id="username" name="username" class="page-input" required="required" type="text" placeholder="Потребителско име"/>
            <input id="password" name="password" class="page-input" required="required" type="password" placeholder="Парола" /> 
                    
			<div class="page-actions">
				<button type="submit" class="page-button page-button-active" name="login">Вход</button>
				<a href=<?php echo ROOT."register" ?> class="page-link">Създаване на профил</a>
			</div>
        </form>
    </div>
</body>

</html>