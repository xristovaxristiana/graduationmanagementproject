-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12 фев 2020 в 20:51
-- Версия на сървъра: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `graduation_db`
--

CREATE DATABASE `graduation_db`;

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role` enum('student','admin','administrator') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'admin'),
(2, 'georgi', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'student'),
(3, 'cvetomilag', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'student'),
(4, 'simonanp', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'student'),
(5, 'administrator', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Структура на таблица `students_import`
--

CREATE TABLE `students_import` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `surname` varchar(256) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `fn` int(5) NOT NULL,
  `specialty` varchar(2) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `degree` enum('bachelor','masters','doctoral','') NOT NULL,
  `grade` decimal(3,2) NOT NULL,
  `username` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `students_import` (`id`, `name`, `surname`, `fn`, `specialty`, `degree`, `grade`, `username`) VALUES
(1, 'Simona', 'Petrova', 81523, 'KN', 'bachelor', '5.00', 'simonanp'),
(2, 'Rosiana', 'Ladzheva', 81461, 'AI', 'bachelor', '6.00', 'rosiana'),
(3, 'Tsvetomila', 'Georgieva', 81542, 'KN', 'masters', '6.00', 'cvetomilag');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students_import`
--
ALTER TABLE `students_import`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fn` (`fn`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students_import`
--
ALTER TABLE `students_import`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Table structure for table `attended`
--

CREATE TABLE `attended` (
  `attended_id` int(11) NOT NULL,
  `attended` tinyint(1) NOT NULL,
  `username` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `attended` (`attended_id`, `attended`, `username`) VALUES
(1, 1, 'cvetomilag'),
(2, 1, 'rosiana'),
(3, 0, 'simonanp');

--
-- Dumping data for table `attended`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attended`
--
ALTER TABLE `attended`
  ADD PRIMARY KEY (`attended_id`),
  ADD KEY `graduation_db_fkey` (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attended`
--
ALTER TABLE `attended`
  ADD CONSTRAINT `graduation_db_fkey` FOREIGN KEY (`username`) REFERENCES `students_import` (`username`);

--
-- Table structure for table `confirmation`
--

CREATE TABLE `confirmation` (
  `confirmation_id` int(11) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `username` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `confirmation` (`confirmation_id`, `confirmed`, `username`) VALUES
(1, 1, 'rosiana'),
(2, 1, 'simonanp'),
(3, 1, 'cvetomilag');

--
-- Dumping data for table `confirmation`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `confirmation`
--
ALTER TABLE `confirmation`
  ADD PRIMARY KEY (`confirmation_id`),
  ADD KEY `graduation_fkey` (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `confirmation`
--
ALTER TABLE `confirmation`
  ADD CONSTRAINT `graduation_fkey` FOREIGN KEY (`username`) REFERENCES `students_import` (`username`);

--
-- Структура на таблица `gowns`
--

CREATE TABLE `gowns` (
  `id` int(11) NOT NULL,
  `recieved` tinyint(4) NOT NULL DEFAULT 0,
  `fn` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gowns`
--
ALTER TABLE `gowns`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gowns`
--
ALTER TABLE `gowns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `caps` (
  `id` int(11) NOT NULL,
  `recieved` tinyint(1) DEFAULT 0,
  `fn` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `caps`
--
ALTER TABLE `caps`
  ADD PRIMARY KEY (`id`);
COMMIT;